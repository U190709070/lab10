package stack;

import java.util.ArrayList;

public interface Stack {

    public void push(Object item);

    public Object pop ();

    public boolean empty();
}
