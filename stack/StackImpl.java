package stack;

public class StackImpl implements Stack{

    StackItem top;
    int size;

    public StackImpl() {
        this.top = null;
        this.size = 0;
    }

    @Override
    public void push(Object item) {
        StackItem oldTop = top;
        top = new StackItem(item, oldTop);
        size++;
    }

    @Override
    public Object pop() {
        Object item = top.item;
        top = top.next;
        return item;
    }

    @Override
    public boolean empty() {

        return top == null;
    }


}
